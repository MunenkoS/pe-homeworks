


function createNewUser() {
    const newUser = {};


    newUser.lastName = prompt('Enter your last name');
    newUser.firstName = prompt('Enter your name');
    newUser.birthday = prompt('Enter your date of birth like: dd.mm.yyyy');
    let dateParts = newUser.birthday.split('.');
    let userDate = new Date(dateParts[2], dateParts[1], dateParts[0]);

    let todayDate = new Date();



    return {
        newUser,
        getLogin: function () {
            let newLogin = newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase();
            return newLogin;
        },
        getAge: function () {
            let age = todayDate - userDate;
            return age / 31540000000;

        },
        getPassword: function () {
            let password = newUser.firstName.charAt(0).toUpperCase() + newUser.lastName.toLowerCase() + dateParts[2];
            return password
        },
    }
}
let newUser = createNewUser();
console.log(`Created login: ${newUser.getLogin()}`);
console.log(`Created password ${newUser.getPassword()}`);
console.log(`Created age ${newUser.getAge()}`);


