let ul = document.createElement('ul');
function converter(arr) {
    let elem = arr.map(function (value) {
        return `<li>${value}</li>`
    });
    return elem;
}
let arrayLi = converter(['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']);

ul.innerHTML = arrayLi.join('');
document.body.append(ul);